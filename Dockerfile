# Utilisez une image de base Java 11
FROM adoptopenjdk:11-jdk-hotspot

# Définissez le répertoire de travail
WORKDIR /app

# Copiez le fichier JAR du projet Spring dans le conteneur
COPY ./target/angular-spring-3.0.0.jar /app

# Exposez le port sur lequel votre application Spring écoute
EXPOSE 8080

# Commande de démarrage de votre application Spring
CMD ["java", "-jar", "angular-spring-3.0.0.jar"]
